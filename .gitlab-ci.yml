include:
  - project: "ci-tools/container-image-ci-templates"
    file: "kaniko-image.gitlab-ci.yml"
    ref: master
  - template: Jobs/SAST.gitlab-ci.yml
  - template: Jobs/Secret-Detection.gitlab-ci.yml

stages:
  - install_dependencies
  - test
  - build_container
  - redeploy

variables:
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"
  PRE_COMMIT_HOME: .cache/pre-commit
  POETRY_HOME: "/opt/poetry"
  POETRY_VERSION: "1.5.0"
  PRE_COMMIT_VERSION: "3.0.0"

.cache_python_template:
  image: python:3.10
  cache:
    - key:
        files:
          - poetry.lock
      paths:
        - .cache/pip
        - .venv

.redeploy_paas_template:
  stage: redeploy
  image: gitlab-registry.cern.ch/paas-tools/openshift-client:latest
  variables:
    REGISTRY_APP_NAME: backlog-processing
    OPENSHIFT_SERVER: https://api.paas.okd.cern.ch # use https://openshift-dev.cern.ch for a Test site
    OPENSHIFT_APP_NAME: backlogprocessing # this is the name of the ImageStream/DeploymentConfig objects created by oc new-app. Typically, this will be the name of the GitLab project.
  script:
    - "oc import-image $OPENSHIFT_APP_NAME --all --server=$OPENSHIFT_SERVER --namespace=$OPENSHIFT_NAMESPACE --from=registry.cern.ch/$REGISTRY_APP_NAME/$IMAGE --confirm --token=$OPENSHIFT_TOKEN"
    # wait a bit for redeployment to happen then monitor the deployment status
    - "sleep 30s && oc rollout status deploy/$OPENSHIFT_APP_NAME --server=$OPENSHIFT_SERVER --namespace $OPENSHIFT_NAMESPACE --token=$OPENSHIFT_TOKEN"

install_pip:
  extends: .cache_python_template
  stage: install_dependencies
  script:
    - python -V
    - pip install poetry==$POETRY_VERSION
    - poetry --version
    - poetry install

pre_commit:
  image: python:3.10
  # Will run the tests with coverage.
  stage: test
  before_script:
    - pip install pre-commit==$PRE_COMMIT_VERSION
  script:
    - pre-commit run --all-files
  cache:
    - key:
        files:
          - .pre-commit-config.yaml
      paths:
        - .cache/pre-commit

test_backend:
  extends: .cache_python_template
  # Will run the tests with coverage.
  stage: test
  before_script:
    - source .venv/bin/activate
  script:
    - cp app/config/config.sample.py app/config/config.py
    - pytest
  needs:
    - install_pip

build_container:
  only:
    - qa
    - main
    - test
  stage: build_container
  extends: .build_kaniko
  variables:
    PUSH_IMAGE: "true"
    REGISTRY_IMAGE_PATH: "registry.cern.ch/backlog-processing/$CI_COMMIT_REF_NAME:$CI_COMMIT_TAG"

redeploy_qa:
  extends: .redeploy_paas_template
  environment:
    name: qa
  variables:
    OPENSHIFT_NAMESPACE: backlog-processing-qa # this is the name of your Openshift project (i.e. the site name)
    OPENSHIFT_TOKEN: $IMAGE_IMPORT_TOKEN_QA
    IMAGE: qa
  only:
    - qa # production or qa, the branch you want to publish

redeploy_prod:
  extends: .redeploy_paas_template
  environment:
    name: main
  variables:
    OPENSHIFT_NAMESPACE: backlog-processing # this is the name of your Openshift project (i.e. the site name)
    OPENSHIFT_TOKEN: $IMAGE_IMPORT_TOKEN_PROD
    IMAGE: main
  only:
    - main # main or qa, the branch you want to publish
