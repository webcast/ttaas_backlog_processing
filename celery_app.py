import urllib3

from app.app_factory import init_celery
from app.utils.logger import setup_logs

urllib3.disable_warnings()

# Initialize the Celery worker to run background tasks
celery = init_celery()

print("Initializing logs...", end="")
setup_logs(celery.app, "job", to_file=celery.app.config.get("LOG_FILE_ENABLED", False))
print("ok")
