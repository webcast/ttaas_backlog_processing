# 1. Generate the deploy token for Gitlab CI

1. Run the following commands in Openshift

```bash
oc create serviceaccount gitlabci-deployer
```

```bash
oc policy add-role-to-user edit -z gitlabci-deployer
```

2. Create a Secret in Openshift and extract the token from the CLI:

```yaml
apiVersion: v1
kind: Secret
metadata:
  name: gitlabci-deployer-token
  annotations:
    kubernetes.io/service-account.name: gitlabci-deployer
type: kubernetes.io/service-account-token
```

```bash
oc get secret gitlabci-deployer-token -o jsonpath='{.data.token}' | base64 -d
```

1. Paste the generated token in Gitlab `Settings/CI/CD/Variables` as `IMAGE_IMPORT_TOKEN_<PROD|QA|TEST>`.

# 2. Give access OKD4 to the registry

1. In registry.cern.ch create a robot account for the project and copy the username and the password.
2. In OKD4, create a pull image secret called `registry.cern.ch` with the following content:
   1. URL: https://registry.cern.ch
   2. Username: The robot account username
   3. Password: The robot account password
3. Go to service accounts and add the recently created `registry.cern.ch` pull secret to the user `default`.

# 2. Add the robot account to Openshift

1. Go to Openshift (developer) -> Secrets
2. Add a "Image Pull Secret"
3. Set registry.cern.ch, username and password (credentials from the robot account)

Now, you need to add the "image pull secret" to the default and the deployer users.

1. Change to "Administrator mode" -> User management -> Service accounts.
2. Select the "default" account and, under the Yaml view, set the pull secret like follows:

```yaml
imagePullSecrets:
  ...
  - name: registry.cern.ch
```

Repeat for the "deployer" user.

# 3. Add the robot account to Gitlab CI

On Settings -> CI -> Variables, set the username and the password of the robot account:

- CI_REGISTRY_USER
- CI_REGISTRY_PASSWORD

These will be used by Kaniko to build and deploy the Docker image on registry.cern.ch.

# 4. Create and populate the config maps

1. app-config: The app configuration
2. server-config: The uwsgi server config

These config maps are mapped to the encoders deployment (webapp) and to the celery workers deployments.

# 5. Create OIDC applications

Two OIDC applications are needed. One for the frontend and one to the backend.

- Frontend: Set "the application cannot store token securely"
- Backend: Client ID + Secret

Populate these values on the configmap.

# 6. Set the environment variables in Gitlab CI

1. CI_REGISTRY_PASSWORD: Password for the registry.cern.ch robot account
2. CI_REGISTRY_USER: User of the registry.cern.ch robot account
3. IMAGE_IMPORT_TOKEN_PROD: Import token generated in step 1
4. IMAGE_IMPORT_TOKEN_QA: Import token generated in step 1
5. IMAGE_IMPORT_TOKEN_TEST: Import token generated in step 1

# 7. Create the redis deployment

Add a redis pod to the application by clicking on the "+" button and then selecting "Redis" on the application's list.

You can auto generate the password and then populate the configmap with its value.

# 8. Create the Openshift objects

Once all the settings are configured, you can create the Openshift objects (The application deployments, service and route)
by restoring them from the `yaml` files.

## Service

```bash
oc create -f okd4/webapp-service.yaml
```

## Route

```bash
oc create -f okd4/webapp-route.yaml
```

## Main application (Deployment encoders)

```bash
oc create -f okd4/backlogprocessing.yaml
```
