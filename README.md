# Backlog processing tool

The application expects an csv file containing the data to be treated, having this format:

```
Recid,Title,EOS File location
1405039,CMS meetings,root://eosmedia.cern.ch//eos/media/cds/prod/videos/files/93/eb/54c9-215e-4b8d-8479-2d7ab2e9598b/data
43130,Un Grand Jour,root://eosmedia.cern.ch//eos/media/cds/prod/videos/files/42/29/175a-723b-40da-bf92-75c8c23dd6fd/data
43117,Eye on Research : The Particle Hunters,root://eosmedia.cern.ch//eos/media/cds/prod/videos/files/8e/dd/d6b0-a60e-4da6-a41c-82bc9a4706b1/data

```

## Commands

Available commands:

```
(.venv) rgaspar@rubens-MacBook-Pro-2 encoders-manager % flask transcription

Commands:
  create-account
  import-file
  ingest-geteditor
  ingest-getlan
  ingest-getlan-jobs
  ingest-getvideotype-jobs
  ingest-ttaas
  ingest-ttaas-jobs

```

To upload a csv file:

```
% flask transcription import-file video_files_for_ttaas.csv
```

The file, could be too big, so instead to pass it as a configmap in Openshift, please do a copy to the container:

```
% ~/Downloads/oc get pods
NAME                                 READY   STATUS    RESTARTS   AGE
backlogprocessing-59b88b54d9-4gprf   3/3     Running   0          23h
whisper-7ddb6598df-rv96j             1/1     Running   0          29m

% ~/Downloads/oc rsync <local-dir> <pod-name>:/<remote-dir> -c <container-name>

% ~/Downloads/oc rsync /Users/XXX/Documents/gitprojects/ttaas_backlog_processing/files backlogprocessing-59b88b54d9-4gprf:/opt/app-root/src -c container

```

To create an account to interact with backend:

```
(.venv) rgaspar@rubens-MacBook-Pro-2 encoders-manager % flask transcription create-account

Usage: flask transcription create-account [OPTIONS] NAME_ACCOUNT TOKEN
Try 'flask transcription create-account --help' for help.

Error: Missing argument 'NAME_ACCOUNT'.
```

To test the lenguage recognition service, where UPLOAD_ID is the primary key in the database for the upload file:

```
(.venv) rgaspar@rubens-MacBook-Pro-2 encoders-manager % flask transcription ingest-getlan

Usage: flask transcription ingest-getlan [OPTIONS] UPLOAD_ID
Try 'flask transcription ingest-getlan --help' for help.

Error: Missing argument 'UPLOAD_ID'.
```

To ingest on TTaaS, where UPLOAD_ID is the primary key in the database for the upload file e.g. 123:

```
(.venv) rgaspar@rubens-MacBook-Pro-2 encoders-manager % flask transcription ingest-ttaas

Usage: flask transcription ingest-ttaas [OPTIONS] UPLOAD_ID
Try 'flask transcription ingest-ttaas --help' for help.

Error: Missing argument 'UPLOAD_ID'.
```

To ingest a number of jobs, files that havent been ingested yet:

```
(.venv) rgaspar@rubens-MacBook-Pro-2 encoders-manager % flask transcription ingest-ttaas-jobs

Usage: flask transcription ingest-ttaas-jobs [OPTIONS] N_JOBS
Try 'flask transcription ingest-ttaas-jobs --help' for help.

Error: Missing argument 'N_JOBS'.
```

## Whisper

In order to retrieve the language from a media we use [Whisper ASR Webservice](https://github.com/ahmetoner/whisper-asr-webservice).

We run the CPU version of the image:

```
--cpu
docker run -d -p 9000:9000 -e ASR_MODEL=base -e ASR_ENGINE=openai_whisper onerahmet/openai-whisper-asr-webservice:latest
--gpu
docker run -d --gpus all -p 9000:9000 -e ASR_MODEL=base -e ASR_ENGINE=openai_whisper onerahmet/openai-whisper-asr-webservice:latest-gpu
```

And the endpoint is access at http:9000. Documentation is accessible [here.](https://ahmetoner.com/whisper-asr-webservice/)
