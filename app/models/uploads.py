import enum
import os
from datetime import datetime

from sqlalchemy import String
from sqlalchemy.orm import Mapped, mapped_column

from app.daos.account import AccountDAO
from app.extensions import db
from app.models.base import ModelBase


class AudioLanguage(enum.Enum):
    ENGLISH = "en"
    FRENCH = "fr"
    OTHER = "other"


class TranscriptionStates(enum.Enum):
    NOT_REQUESTED = "NOT_REQUESTED"
    REQUESTING = "REQUESTING"
    WAITING = "WAITING"
    TRANSCRIPTION_FINISHED = "TRANSCRIPTION_FINISHED"
    TRANSLATION_FINISHED = "TRANSLATION_FINISHED"
    COMPLETED_CALLBACK_FAILED = "COMPLETED_CALLBACK_FAILED"
    COMPLETED_EMAIL_FAILED = "COMPLETED_EMAIL_FAILED"
    ERROR = "ERROR"
    COMPLETED = "COMPLETED"
    TRY_LATER = "TRY_LATER"
    LANGUAGE_NOT_SUPPORTED = "LANGUAGE_NOT_SUPPORTED"


class Upload(ModelBase):
    namefile: Mapped[str] = mapped_column(String(255), nullable=False)
    pathtofile: Mapped[str] = mapped_column(String(512), nullable=False)
    editor_ro: Mapped[str] = mapped_column(String(512), nullable=True)
    editor_rw: Mapped[str] = mapped_column(String(512), nullable=True)
    fulloriginalpath: Mapped[str] = mapped_column(
        String(512), unique=True, nullable=False
    )
    title: Mapped[str] = mapped_column(String(512), nullable=False)
    recid: Mapped[str] = mapped_column(String(255), nullable=False)
    state: Mapped[TranscriptionStates] = mapped_column(
        default=TranscriptionStates.NOT_REQUESTED
    )
    mediaid: Mapped[str] = mapped_column(String(255))
    language: Mapped[AudioLanguage] = mapped_column(nullable=True)
    created_on: Mapped[datetime] = mapped_column(default=db.func.now())
    updated_on: Mapped[datetime] = mapped_column(
        default=db.func.now(), onupdate=db.func.now()
    )
    uploaded: Mapped[bool] = mapped_column(db.Boolean, default=False)
    account_id: Mapped[int] = mapped_column(db.ForeignKey("account.id"))
    # account: Mapped["Account"] = relationship("Account", back_populates="uploads", foreign_keys=account_id, post_update=True)

    def __str__(self):
        return self.fulloriginalpath + " " + self.state.value + " " + self.recid

    @staticmethod
    def _pathAndNameEOS(fullpath):
        # root://eosmedia.cern.ch//eos/media/cds/prod/videos/files/93/eb/54c9-215e-4b8d-8479-2d7ab2e9598b/data
        pathnoeoscell = fullpath
        arrdouble = fullpath.split("//")
        if len(arrdouble) == 3:
            pathnoeoscell = arrdouble[2]
        root, file_name = os.path.split(pathnoeoscell)
        return "/" + root, file_name

    @staticmethod
    def create(fulloriginalpath, recid, title, account_name):
        accountid = AccountDAO.get_id_by_name(name=account_name)
        if accountid:
            upload_object = db.session.execute(
                db.select(Upload).filter_by(fulloriginalpath=fulloriginalpath)
            ).scalar_one_or_none()
            if not upload_object:
                pathtofile, namefile = Upload._pathAndNameEOS(fulloriginalpath)
                upload_object = Upload(
                    fulloriginalpath=fulloriginalpath,
                    recid=recid,
                    title=title,
                    account_id=accountid,
                    pathtofile=pathtofile,
                    namefile=namefile,
                    mediaid=f"{recid}_{account_name}",
                )
                db.session.add(upload_object)
                db.session.commit()
            return upload_object
