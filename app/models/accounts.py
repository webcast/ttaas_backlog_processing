from datetime import datetime
from typing import TYPE_CHECKING, List

from sqlalchemy import String
from sqlalchemy.orm import Mapped, mapped_column, relationship

from app.extensions import db
from app.models.base import ModelBase

if TYPE_CHECKING:
    from app.models.uploads import Upload


class Account(ModelBase):
    name: Mapped[str] = mapped_column(String(255), unique=True, nullable=False)
    token: Mapped[str] = mapped_column(String(512), unique=True, nullable=False)
    apikey: Mapped[str] = mapped_column(String(512), unique=True, nullable=False)
    created_on: Mapped[datetime] = mapped_column(default=db.func.now())
    updated_on: Mapped[datetime] = mapped_column(
        default=db.func.now(), onupdate=db.func.now()
    )

    uploads: Mapped[List["Upload"]] = relationship(
        "Upload", backref="account", lazy=True
    )

    def __str__(self):
        return self.name

    @staticmethod
    def create(name, token, apikey):
        account_object = db.session.execute(
            db.select(Account).filter_by(name=name)
        ).scalar_one_or_none()
        if not account_object:
            account_object = Account(name=name, token=token, apikey=apikey)
            db.session.add(account_object)
            db.session.commit()
        return account_object
