from sqlalchemy.orm import Mapped, mapped_column

from app.extensions import db


class ModelBase(db.Model):  # type: ignore
    __abstract__ = True

    id: Mapped[int] = mapped_column(db.Integer, primary_key=True)
