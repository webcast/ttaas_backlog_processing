import enum
import logging

from flask import Response, request
from flask_restx import Namespace, Resource

from app.daos.uploads import UploadDAO
from app.extensions import csrf
from app.models.uploads import TranscriptionStates
from app.services.transcription.transcription_service import TranscriptionService

logger = logging.getLogger("webapp.ttaas_api")

namespace = Namespace("ttaas", description="TTAAS callbacks related operations")


class TtaasCallbackType(enum.Enum):
    FINISHED = "FINISHED"
    ALREADY_EXISTS = "ALREADY_EXISTS"
    ERROR = "ERROR"
    CAPTION_UPDATED = "CAPTION_UPDATED"


@namespace.route("/")
class TtaasCallbackEndpoint(Resource):
    decorators = [csrf.exempt]

    @namespace.doc("ttaas_callback")
    def post(self) -> Response:
        logger.info("TTAAS API callback endpoint")
        logger.debug(request.json)
        callback_type = request.json["callbackType"]
        contribution_id = request.json["upload_id"][0]
        media_id = request.json["mediaId"]

        contribution = UploadDAO.get_by_id(id=contribution_id)

        if callback_type == TtaasCallbackType.ALREADY_EXISTS.value:
            logger.info("Received already-exists from ttaas service")
            state = request.json["state"]
            media_id = request.json["existingMediaId"]
            if (
                state == TranscriptionStates.COMPLETED_CALLBACK_FAILED.value
                or state == TranscriptionStates.COMPLETED_EMAIL_FAILED.value
                or state == TranscriptionStates.TRANSLATION_FINISHED.value
                or state == TranscriptionStates.TRANSCRIPTION_FINISHED.value
                or state == TranscriptionStates.COMPLETED.value
            ):
                # update the field, so update on gets the new date.
                UploadDAO.set_transcription_status(
                    contribution.id,
                    TranscriptionStates.COMPLETED.value,
                    media_id=media_id,
                )
                ttaas = TranscriptionService(contribution=contribution, logger=logger)
                ttaas.request_transcription_update()
            else:
                UploadDAO.set_transcription_status(
                    contribution.id,
                    TranscriptionStates.TRY_LATER.value,
                    media_id=media_id,
                )

        if callback_type == TtaasCallbackType.FINISHED.value:
            logger.info("Received finished from ttaas service")
            UploadDAO.set_transcription_status(
                contribution.id,
                TranscriptionStates.COMPLETED.value,
                media_id=media_id,
            )
            ttaas = TranscriptionService(contribution=contribution, logger=logger)
            ttaas.request_transcription_update()

        if callback_type == TtaasCallbackType.ERROR.value:
            logger.info("Received error callback from ttaas service")
            UploadDAO.set_transcription_status(
                contribution.id,
                TranscriptionStates.ERROR.value,
                media_id=media_id,
            )

        if callback_type == TtaasCallbackType.CAPTION_UPDATED.value:
            logger.info("Received caption-updated from ttaas service")

        return "", 200
