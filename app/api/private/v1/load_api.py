import logging

from flask import Blueprint
from flask_restx import Api

from app.api.private.v1.namespaces.callbacks import namespace as callback_namespace

# from app.api.private.v1.namespaces.encoder_groups import (
#     namespace as encoder_groups_namespace,
# )
# from app.api.private.v1.namespaces.encoders import namespace as encoders_namespace
# from app.api.private.v1.namespaces.get_status import namespace as get_status_namespace
# from app.api.private.v1.namespaces.indico_events import (
#     namespace as indico_events_namespace,
# )
# from app.api.private.v1.namespaces.recording import namespace as recording_namespace
# from app.api.private.v1.namespaces.stream import namespace as stream_namespace
# from app.api.private.v1.namespaces.stream_recording import (
#     namespace as stream_recording_namespace,
# )
# from app.api.private.v1.namespaces.test import namespace as test_namespace


logger = logging.getLogger("webapp")


def load_api_namespaces() -> Blueprint:
    """Loadd all the namespaces from the private API.
    Private API is the API that is only accessible to
    the application internal use fro CRUD operations.

    Returns:
        Blueprint: The blueprint for the API
    """
    blueprint = Blueprint("private_api", __name__)
    api = Api(blueprint)
    # api.add_namespace(indico_events_namespace)
    # api.add_namespace(encoder_groups_namespace)
    api.add_namespace(callback_namespace)
    # api.add_namespace(stream_namespace)
    # api.add_namespace(recording_namespace)
    # api.add_namespace(stream_recording_namespace)
    # api.add_namespace(encoders_namespace)
    # api.add_namespace(get_status_namespace)

    return blueprint
