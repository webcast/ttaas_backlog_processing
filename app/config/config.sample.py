# Whether or not to use the werkzeug proxy for the requests. Required on Openshift.
# Values: boolean (Default: False)
USE_PROXY = False
#
#
IS_DEV = False
# Secret key for the application. Used for the CSRF protection.
# Values: string
SECRET_KEY = "secret"
# Role name for the admin users
# Values: string (Default: "admins")
ADMIN_ROLE = "admins"
# Indico base URL.
# Values: string (Default: "https://indico.cern.ch")
INDICO_ENDPOINT = "https://indico.cern.ch"
# Indico access token. Used to access the Indico API. Generated on the profile page.
# Values: string
INDICO_ACCESS_TOKEN = "<TOKEN>"
# Timezone of the application
# Values: string (Default: "Europe/Zurich")
APPLICATION_TIMEZONE = "Europe/Zurich"
# Log level for the application
# Values: string (Default: "DEBUG")
LOG_LEVEL = "DEBUG"
# Whether or not to enable the log file
# Values: boolean (Default: False)
LOG_FILE_ENABLED = False
# Type of the database to use
# Values: mysql|postgresql|sqlite
# Default: sqlite
DB_ENGINE = "sqlite"
# Username of the database
# Values: string (Default: "root")
# Default: root
DB_USER = "root"
# Password of the database
# Values: string (Default: "root")
# Default: root
DB_PASS = "root"
# Hostname of the database
# Values: string (Default: "localhost")
# Default: localhost
DB_SERVICE_NAME = "127.0.0.1"
# Port of the database
# Values: string (Default: "3306")
# Default: 3306
DB_PORT = "3306"
# Name of the database
# Values: string (Default: "encoders")
# Default: encoders
DB_NAME = "encoders"
# Whether to enable the cache or not
# Values: boolean (Default: True)
CACHE_ENABLE = True
# Type of the cache to use
# Values: https://flask-caching.readthedocs.io/en/latest/index.html#configuring-flask-caching
CACHE_TYPE = "SimpleCache"  # Flask-Caching related configs
# Default timeout for the cache
# Values: integer (Default: 300)
CACHE_DEFAULT_TIMEOUT = 300

# Whether or not to enable the Sentry integration
# Value: boolean (Default: False)
ENABLE_SENTRY = False
# Sentry DSN
# Value: string
SENTRY_DSN = ""
# Sentry environment
# Value: string (Default: "dev")
SENTRY_ENVIRONMENT = "dev"
