# Whether or not to use the werkzeug proxy for the requests. Required on Openshift.
# Values: boolean (Default: False)
USE_PROXY = True
# Secret key for the application. Used for the CSRF protection.
# Values: string
SECRET_KEY = b"<SECRET>"
#
#
IS_DEV = True
# Timezone of the application
# Values: string (Default: "Europe/Zurich")
APPLICATION_TIMEZONE = "Europe/Zurich"
# Log level for the application
# Values: string (Default: "DEBUG")
LOG_LEVEL = "DEBUG"
# Whether or not to enable the log file
# Values: boolean (Default: False)
LOG_FILE_ENABLED = False
# Type of the database to use
# Values: mysql|postgresql|sqlite
# Default: sqlite
DB_ENGINE = "sqlite"
# Username of the database
# Values: string (Default: "root")
# Default: root
DB_USER = "<ACCOUNT>"
# Password of the database
# Values: string (Default: "root")
# Default: root
DB_PASS = "12345"
# Hostname of the database
# Values: string (Default: "localhost")
# Default: localhost
DB_SERVICE_NAME = "localhost"
# Port of the database
# Values: string (Default: "3306")
# Default: 3306
DB_PORT = "5545"
# Name of the database
# Values: string (Default: "encoders")
# Default: encoders
DB_NAME = "<ACCOUNT>"
# Whether to enable the cache or not
# Values: boolean (Default: True)
CACHE_ENABLE = True
# Type of the cache to use
# Values: https://flask-caching.readthedocs.io/en/latest/index.html#configuring-flask-caching
CACHE_TYPE = "SimpleCache"  # Flask-Caching related configs
# Default timeout for the cache
# Values: integer (Default: 300)
CACHE_DEFAULT_TIMEOUT = 300
ENABLE_SENTRY = False
# Sentry DSN
# Value: string
SENTRY_DSN = "https://XXXsentry.web.cern.ch/7"
# Sentry environment
# Value: string (Default: "dev")
SENTRY_ENVIRONMENT = "qa"


# NEW
# CALLBACK_URL that TLP will run to inform of state changes on a media
TTAAS_SERVICE_CALLBACK_URL = "https://XXXX/"
# URL to contact TTaaS service
TTAAS_SERVICE_ENDPOINT = "https://<SERVER>.web.cern.ch"
# URL to contact Whisper service.
WHISPER_ENDPOINT = "http://localhost:9000/"
# Needed for CDS videos as they dont have extension. So they are downloaded and them uploaded with an extension.
STAGING_AREA = "/tmp/"
# Required to connect to TLP
MLLP_CONFIG = {
    "API_BASE": "https://<SERVER>.cern.ch/api/v3/",
    "EDITOR_BASE": "https://<SERVER>.cern.ch/player",
}
# Required forr MLLP operations
MLLP_ADMIN_ACCOUNT = "<ACCOUNT>"
