from app.extensions import db
from app.models.accounts import Account


class AccountDAO:
    @staticmethod
    def get_by_name(name: str) -> Account:
        account = db.session.execute(
            db.select(Account).filter_by(name=name)
        ).scalar_one_or_none()
        return account

    @staticmethod
    def get_by_id(account_id: int) -> Account:
        account = db.session.execute(
            db.select(Account).filter_by(id=account_id)
        ).scalar_one_or_none()
        return account

    @staticmethod
    def get_id_by_name(name: str) -> int:
        account = db.session.execute(
            db.select(Account).filter_by(name=name)
        ).scalar_one_or_none()
        return account.id

    # @staticmethod
    # def get_all() -> List[IndicoEvent]:
    #     events = db.session.execute(db.select(IndicoEvent)).scalars().all()
    #     return events

    # @staticmethod
    # def get_by_id(internal_id: int) -> IndicoEvent:
    #     event = db.session.execute(
    #         db.select(IndicoEvent).filter_by(id=internal_id)
    #     ).scalar_one_or_none()
    #     return event

    # @staticmethod
    # def get_all_no_reminder_sent() -> List[IndicoEvent]:
    #     events = (
    #         db.session.execute(db.select(IndicoEvent).filter_by(reminder_sent=False))
    #         .scalars()
    #         .all()
    #     )
    #     return events

    # @staticmethod
    # def get_recording_count_from_year(year: int) -> int:
    #     query = (
    #         db.select(IndicoEvent)
    #         .filter(extract("year", IndicoEvent.recording_start_datetime) == year)
    #         .filter_by(is_recorded=True)
    #         .filter_by(event_type=EventType.RECORDING)
    #     )
    #     events = len(db.session.execute(query).scalars().all())
    #     return events

    # @staticmethod
    # def get_stream_count_from_year(year: int) -> int:
    #     query = (
    #         db.select(IndicoEvent)
    #         .filter(extract("year", IndicoEvent.recording_start_datetime) == year)
    #         .filter_by(is_streamed=True)
    #         .filter_by(event_type=EventType.STREAM)
    #     )
    #     events = len(db.session.execute(query).scalars().all())
    #     return events

    # @staticmethod
    # def get_stream_recording_count_from_year(year: int) -> int:
    #     query = (
    #         db.select(IndicoEvent)
    #         .filter(extract("year", IndicoEvent.recording_start_datetime) == year)
    #         .filter_by(is_streamed=True, is_recorded=True)
    #         .filter_by(event_type=EventType.STREAM_RECORDING)
    #     )
    #     events = len(db.session.execute(query).scalars().all())
    #     return events

    # @staticmethod
    # def get_all_with_auto_stop() -> List[IndicoEvent]:
    #     events = (
    #         db.session.execute(
    #             db.select(IndicoEvent).filter(
    #                 IndicoEvent.auto_stop_datetime.isnot(None)
    #             )
    #         )
    #         .scalars()
    #         .all()
    #     )
    #     return events

    # @staticmethod
    # def get_by_indico_id(indico_id: str):
    #     event = db.session.execute(
    #         db.select(IndicoEvent).filter_by(indico_id=indico_id)
    #     ).scalar_one_or_none()
    #     return event

    # @staticmethod
    # def get_next_events_admin() -> List[IndicoEvent]:
    #     today = datetime.today().date()
    #     midnight_datetime = datetime.combine(today, datetime.min.time())
    #     tomorrow_datetime = datetime.combine(today, datetime.max.time())
    #     query = (
    #         db.select(IndicoEvent)
    #         .filter(
    #             and_(
    #                 IndicoEvent.start_datetime >= midnight_datetime,
    #                 IndicoEvent.start_datetime <= tomorrow_datetime,
    #             )  # Starting today
    #             | and_(
    #                 IndicoEvent.start_datetime <= midnight_datetime,
    #                 IndicoEvent.end_datetime >= midnight_datetime,
    #             )  # Starting before today and ending after today
    #             | (IndicoEvent.is_locked == True)  # noqa
    #             | (IndicoEvent.is_running == True)  # noqa)
    #         )
    #         .order_by(desc(IndicoEvent.end_datetime))
    #     )

    #     events = db.session.execute(query).scalars().all()

    #     return events

    # @staticmethod
    # def get_next_events_from_rooms(rooms_with_access: List[int]) -> List[IndicoEvent]:
    #     today = datetime.today().date()
    #     midnight_datetime = datetime.combine(today, datetime.min.time())
    #     tomorrow_datetime = datetime.combine(today, datetime.max.time())

    #     query = (
    #         db.select(IndicoEvent)
    #         .filter(
    #             and_(
    #                 IndicoEvent.start_datetime >= midnight_datetime,
    #                 IndicoEvent.start_datetime <= tomorrow_datetime,
    #             )  # Starting today
    #             | and_(
    #                 IndicoEvent.start_datetime <= midnight_datetime,
    #                 IndicoEvent.end_datetime >= midnight_datetime,
    #             )  # Starting before today and ending after today
    #             | (IndicoEvent.is_locked == True)  # noqa
    #             | (IndicoEvent.is_running == True)  # noqa)
    #         )
    #         .filter(IndicoEvent.room_id.in_(rooms_with_access))
    #         .order_by(desc(IndicoEvent.end_datetime))
    #     )
    #     events = db.session.execute(query).scalars().all()

    #     return events

    # @staticmethod
    # def create(
    #     indico_id,
    #     event_json: dict,
    #     audience: Audience = Audience.PUBLIC,
    #     is_locked: bool = False,
    # ) -> IndicoEvent:
    #     indico_id = event_json["event_id"]
    #     room_object = IndicoEventsDAO.get_or_create_room_from_dict(event_json)

    #     existing_event = db.session.execute(
    #         db.select(IndicoEvent).filter_by(indico_id=indico_id)
    #     ).scalar_one_or_none()

    #     if existing_event:
    #         raise IndicoEventException(
    #             f"The event {indico_id} already exists. Cannot create it again."
    #         )

    #     event = IndicoEvent(indico_id=indico_id)

    #     IndicoEventsDAO.assign_event_attrs(
    #         event_json, room_object, event, audience=audience, is_locked=is_locked
    #     )

    #     db.session.add(event)
    #     db.session.commit()

    #     return event

    # @staticmethod
    # def update(
    #     indico_id,
    #     event_json: dict,
    #     audience: Audience = Audience.PUBLIC,
    #     is_locked: bool = False,
    # ) -> IndicoEvent:
    #     indico_id = event_json["event_id"]
    #     room_object = IndicoEventsDAO.get_or_create_room_from_dict(event_json)

    #     existing_event = db.session.execute(
    #         db.select(IndicoEvent).filter_by(indico_id=indico_id)
    #     ).scalar_one_or_none()

    #     if not existing_event:
    #         raise IndicoEventException(
    #             f"The event {indico_id} doesn't exist. Cannot update it."
    #         )

    #     IndicoEventsDAO.assign_event_attrs(
    #         event_json,
    #         room_object,
    #         existing_event,
    #         audience=audience,
    #         is_locked=is_locked,
    #     )

    #     db.session.add(existing_event)
    #     db.session.commit()

    #     return existing_event

    # @staticmethod
    # def get_or_create_room_from_dict(event_dict: dict):
    #     room_name = event_dict["room"]
    #     room_full_name = event_dict["room_full_name"]
    #     room_object = Room.get_or_create(room_name, room_full_name)
    #     return room_object

    # @staticmethod
    # def assign_event_attrs(
    #     event_json, room_object, event, audience: Audience, is_locked: bool = False
    # ):
    #     event.audience = audience
    #     event.is_locked = is_locked

    #     event.title = event_json["title"]
    #     start_date = event_json["startDate"]["date"]
    #     start_time = event_json["startDate"]["time"]
    #     start_timezone = event_json["startDate"]["tz"]

    #     start_date_time = datetime.strptime(
    #         f"{start_date} {start_time}", "%Y-%m-%d %H:%M:%S"
    #     ).replace(tzinfo=pytz.timezone(start_timezone))

    #     start_datetime = change_timezone(
    #         start_date_time, current_app.config["APPLICATION_TIMEZONE"]
    #     )
    #     event.start_datetime = start_datetime
    #     event.room = room_object

    #     end_date = event_json["endDate"]["date"]
    #     end_time = event_json["endDate"]["time"]
    #     end_timezone = event_json["endDate"]["tz"]

    #     end_date_time = datetime.strptime(
    #         f"{end_date} {end_time}", "%Y-%m-%d %H:%M:%S"
    #     ).replace(tzinfo=pytz.timezone(end_timezone))

    #     end_datetime = change_timezone(
    #         end_date_time, current_app.config["APPLICATION_TIMEZONE"]
    #     )
    #     event.end_datetime = end_datetime

    #     event.event_type = event_json["event_type"]

    #     event.indico_url = event_json["url"]
