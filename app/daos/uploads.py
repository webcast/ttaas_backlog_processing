from datetime import datetime, timedelta

from app.extensions import db
from app.models.uploads import AudioLanguage, Upload


class UploadDAO:
    @staticmethod
    def get_by_recid_and_filename(namefile: str, recid: str, id: int) -> Upload:
        # upload = db.session.execute(
        #    db.select(Upload).filter_by(namefile=namefile, recid=recid)
        # ).scalar_one_or_none()
        return UploadDAO.get_by_id(id)

    @staticmethod
    def get_by_id(id: int) -> Upload:
        upload = db.session.execute(
            db.select(Upload).filter_by(id=id)
        ).scalar_one_or_none()
        return upload

    @staticmethod
    def get_by_state_limit(state: str, limit: int, account_id: int):
        uploads = Upload.query.filter_by(state=state, account_id=account_id).limit(
            limit
        )
        return uploads

    @staticmethod
    def get_by_nolanguage_limit(limit: int, account_id: int):
        uploads = Upload.query.filter_by(language=None, account_id=account_id).limit(
            limit
        )
        return uploads

    @staticmethod
    def set_language(upload_id: int, language: str):
        upload_object = Upload.query.filter_by(id=upload_id).first()
        if language == "en":
            upload_object.language = AudioLanguage.ENGLISH
        elif language == "fr":
            upload_object.language = AudioLanguage.FRENCH
        else:
            upload_object.language = AudioLanguage.OTHER
        db.session.commit()

    @staticmethod
    def get_language(upload_id: int):
        upload_object = Upload.query.filter_by(id=upload_id).first()
        if upload_object.language:
            return upload_object.language
        return None

    @staticmethod
    def set_transcription_status(upload_id, state, media_id):

        upload_object = Upload.query.filter_by(id=upload_id).first()
        upload_object.mediaid = media_id
        upload_object.state = state
        db.session.commit()

    @staticmethod
    def set_editor_links(mediaid: str, editor_link_ro: str, editor_link_rw: str):

        upload_object = Upload.query.filter_by(mediaid=mediaid).first()
        upload_object.editor_ro = editor_link_ro
        upload_object.editor_rw = editor_link_rw
        db.session.commit()

    @staticmethod
    def get_all_records_peraccountid(account_id: int):
        return Upload.query.filter_by(account_id=account_id).count()

    @staticmethod
    def get_all_processed_records_peraccountid(account_id: int):
        return (
            Upload.query.filter_by(account_id=account_id)
            .filter(Upload.state != "NOT_REQUESTED")
            .count()
        )

    @staticmethod
    def get_all_records_per_accountid_ok(account_id: int):
        return (
            Upload.query.filter_by(account_id=account_id)
            .filter(
                Upload.state.in_(
                    [
                        "COMPLETED",
                        "COMPLETED_EMAIL_FAILED",
                        "COMPLETED_CALLBACK_FAILED",
                    ]
                )
            )
            .all()
        )

    @staticmethod
    def get_all_records_per_accountid_gt_id(account_id: int, id: int):
        uploads = (
            Upload.query.filter_by(account_id=account_id)
            .filter(
                Upload.state.in_(
                    [
                        "COMPLETED",
                        "COMPLETED_EMAIL_FAILED",
                        "COMPLETED_CALLBACK_FAILED",
                    ]
                )
            )
            .filter(Upload.id > id)
            .all()
        )
        return uploads

    @staticmethod
    def get_record_by_mediaid(account_id: int, mediaid: str) -> Upload:
        upload = db.session.execute(
            db.select(Upload).filter_by(mediaid=mediaid, account_id=account_id)
        ).scalar_one_or_none()
        return upload

    @staticmethod
    def get_processed_okrecords_withinterval_peraccountid(
        account_id: int, days: int = 0
    ):
        if not days:
            return (
                Upload.query.filter_by(account_id=account_id)
                .filter(
                    Upload.state.in_(
                        [
                            "LANGUAGE_NOT_SUPPORTED",
                            "COMPLETED",
                            "COMPLETED_EMAIL_FAILED",
                            "COMPLETED_CALLBACK_FAILED",
                        ]
                    )
                )
                .count()
            )
        else:
            date_in_past = datetime.now() - timedelta(days=days)
            return (
                Upload.query.filter_by(account_id=account_id)
                .filter(
                    Upload.state.in_(
                        [
                            "LANGUAGE_NOT_SUPPORTED",
                            "COMPLETED",
                            "COMPLETED_EMAIL_FAILED",
                            "COMPLETED_CALLBACK_FAILED",
                        ]
                    )
                )
                .filter(Upload.updated_on >= date_in_past)
                .count()
            )
