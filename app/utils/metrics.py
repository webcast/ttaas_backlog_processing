from app.daos.indico_events import IndicoEventsDAO


def get_recordings_metrics(year: int):
    recorded_events = IndicoEventsDAO.get_recording_count_from_year(year)

    return {
        "recorded_events": recorded_events,
    }


def get_streams_metrics(year: int):
    streamed_events = IndicoEventsDAO.get_stream_count_from_year(year)

    return {
        "streamed_events": streamed_events,
    }


def get_streams_recordings_metrics(year: int):
    streams_recordings = IndicoEventsDAO.get_stream_recording_count_from_year(year)

    return {
        "streamed_recorded_events": streams_recordings,
    }


def get_metrics(year: int):
    recording_metrics = get_recordings_metrics(year)
    streams_metrics = get_streams_metrics(year)
    streams_recordings_metrics = get_streams_recordings_metrics(year)

    return {
        "recordings": recording_metrics,
        "streams": streams_metrics,
        "streams_recordings": streams_recordings_metrics,
    }
