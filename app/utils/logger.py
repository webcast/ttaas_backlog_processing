import logging
import sys
from datetime import datetime
from logging import handlers

import sentry_sdk
from flask import Flask
from pytz import timezone, utc


class SentryTagFilter(logging.Filter):
    def filter(self, record):
        # Extract extra fields (tags) from the record object
        tags = getattr(record, "tags", None)
        if tags and isinstance(tags, dict):
            with sentry_sdk.configure_scope() as scope:
                for key, value in tags.items():
                    scope.set_tag(key, value)
        return True


class CustomFormatter(logging.Formatter):
    def format(self, record):
        # Get the "tags" attribute from the record's extra field
        tags = record.__dict__.get("tags", None)

        # If tags are present, join them with a comma
        if tags:
            tags_str = ", ".join(f"{key}={value}" for key, value in tags.items())
        else:
            tags_str = ""

        # Add the tags string to the log message
        record.tags = tags_str

        # Call the parent formatter to complete the formatting
        return super(CustomFormatter, self).format(record)


def zurich_time(*args):
    utc_dt = utc.localize(datetime.utcnow())
    my_tz = timezone("Europe/Zurich")
    converted = utc_dt.astimezone(my_tz)
    return converted.timetuple()


def setup_logs(app: Flask, logger_name: str, to_stdout=True, to_file=False):
    """
    Set up the logs for the application

    :param app: Flask application where the configuration will be obtained
    :type app: flask.Flask
    :param logger_name: The name of the logger
    :type logger_name: str
    :param to_stdout: Whether or not to log to stdout
    :type to_stdout: bool
    :param to_remote: Whether or not to use remote logging
    :type to_remote: bool
    :return:
    :rtype:
    """
    logger = logging.getLogger(logger_name)

    if app.config["LOG_LEVEL"] == "DEBUG":
        logger.setLevel(logging.DEBUG)

    if app.config["LOG_LEVEL"] == "PROD":
        logger.setLevel(logging.INFO)

    format = (
        "%(asctime)s | %(levelname)s | %(name)s | %(message)s "
        "| %(pathname)s:%(lineno)d | %(funcName)s() | %(tags)s"
    )

    formatter = CustomFormatter(format)

    logging.Formatter.converter = zurich_time

    if to_stdout:
        configure_stdout_logging(
            app, logger, formatter=formatter, log_level=app.config["LOG_LEVEL"]
        )

    if to_file:
        configure_file_logging(app, logger, formatter=formatter, file_name=logger_name)


def configure_stdout_logging(
    app: Flask,
    logger: logging.Logger,
    formatter: logging.Formatter,
    log_level: str = "DEV",
):
    """
    Set up the stdout logging

    :param logger: The logger to be configured
    :type logger: logging.Logger
    :param formatter: Formatter to be used
    :type formatter: logging.Formatter
    :param log_level: The level of the logging: DEV|PROD
    :type log_level: str
    :return:
    :rtype:
    """
    stream_handler = logging.StreamHandler(stream=sys.stdout)

    stream_handler.setFormatter(formatter)
    if log_level == "DEV":
        stream_handler.setLevel(logging.DEBUG)
    if log_level == "PROD":
        stream_handler.setLevel(logging.INFO)

    logger.addHandler(stream_handler)

    if app.config["ENABLE_SENTRY"]:
        sentry_filter = SentryTagFilter()
        stream_handler.addFilter(sentry_filter)

    print(f"Logging {str(logger)} to stdout -> True")


def configure_file_logging(
    app: Flask,
    logger: logging.Logger,
    formatter: logging.Formatter,
    file_name: str = "application",
):
    log_file_path = app.config.get("LOG_FILE_PATH", "/opt/app-root/src/logs")
    log_file_name = f"{log_file_path}/{file_name}.log"
    try:
        file_handler = handlers.TimedRotatingFileHandler(
            log_file_name, when="midnight", interval=7, backupCount=5
        )
        file_handler.setFormatter(formatter)
        if app.config.get("LOG_LEVEL", "DEV") == "DEV":
            file_handler.setLevel(logging.DEBUG)
        if app.config.get("LOG_LEVEL", "DEV") == "PROD":
            file_handler.setLevel(logging.INFO)

        logger.addHandler(file_handler)
        print(f"Logging {str(logger)} to file ({log_file_name}) -> True")

    except OSError as error:
        print(
            f"""It seems there is a problem with the volume.
            Logs will be only on stdout. Error: {str(error)}"""
        )
