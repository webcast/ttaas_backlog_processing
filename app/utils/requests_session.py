import requests
from requests.adapters import HTTPAdapter
from urllib3.util.retry import Retry


def create_requests_session(max_retries=3, backoff_factor=0.5, per_request_delay=0.5):
    session = requests.Session()

    retries = Retry(
        total=max_retries,
        backoff_factor=backoff_factor,
        status_forcelist=[429, 500, 502, 503, 504],
    )

    adapter = HTTPAdapter(max_retries=retries)
    session.mount("http://", adapter)
    session.mount("https://", adapter)

    class RateLimitAdapter(HTTPAdapter):
        def send(self, request, **kwargs):
            import time

            time.sleep(per_request_delay)  # Add a delay between requests
            return super(RateLimitAdapter, self).send(request, **kwargs)

    rate_limit_adapter = RateLimitAdapter(max_retries=retries)
    session.mount("http://", rate_limit_adapter)
    session.mount("https://", rate_limit_adapter)

    return session
