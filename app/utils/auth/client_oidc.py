import datetime
import logging
from http import HTTPStatus

import requests
from authlib.jose import JsonWebKey, jwt
from authlib.jose.errors import ExpiredTokenError
from authlib.oidc.core import ImplicitIDToken, UserInfo
from flask import current_app, g, request
from flask_login import login_user
from flask_restx import abort
from jwt import DecodeError

from app.extensions import db
from app.models.users import User

logger = logging.getLogger("webapp.api")


class ImplicitIDTokenNoNonce(ImplicitIDToken):
    """
    Don't validate the nonce claim as it's not coming with the token
    """

    ESSENTIAL_CLAIMS = ["iss", "sub", "aud", "exp", "iat", "azp"]


def parse_id_token(id_token):
    logger.debug("Parsing id token")

    def load_key(header, payload):
        logger.debug("Loading key")
        jwk_set = requests.get(
            current_app.config["OIDC_CONFIG_CLIENT"]["OIDC_JWKS_URL"]
        ).json()

        key_set = JsonWebKey.import_key_set(jwk_set)
        loaded_key = key_set.find_by_kid(header.get("kid"))

        return loaded_key

    claims_params = {
        "client_id": current_app.config["OIDC_CONFIG_CLIENT"]["OIDC_CLIENT_ID"]
    }
    claims_cls = ImplicitIDTokenNoNonce
    claims_options = {
        "iss": {"values": [current_app.config["OIDC_CONFIG_CLIENT"]["OIDC_ISSUER"]]},
        "nonce": {"validate": lambda x: True},
    }
    logger.debug("Decoding id token")
    claims = jwt.decode(
        id_token,
        key=load_key,
        claims_cls=claims_cls,
        claims_options=claims_options,
        claims_params=claims_params,
    )
    logger.debug("Validating id token")
    claims.validate(leeway=120)
    return UserInfo(claims)


def get_user_frofile_from_token(user_info: UserInfo) -> User:
    """Get the user profile from the token

    Args:
        user_info (UserInfo): [description]

    Returns:
        User: [description]
    """
    profile = {
        "email": user_info.email,
        "first_name": user_info.given_name,
        "last_name": user_info.family_name,
        "username": user_info.sub,
        "roles": user_info.get("cern_roles", []),
    }
    profile["is_admin"] = False
    if current_app.config["ADMIN_ROLE"] in profile["roles"]:
        profile["is_admin"] = True

    query = db.select(User).filter_by(username=profile["username"].strip())
    user = db.session.execute(query).scalar_one_or_none()
    if not user:
        user = User(
            username=profile["username"].strip(),
            email=profile["email"],
            last_name=profile["last_name"],
            first_name=profile["first_name"],
        )
        db.session.add(user)
    user.is_admin = profile["is_admin"]
    user.roles = ", ".join(profile["roles"])
    user.last_login = datetime.datetime.now()
    db.session.commit()
    # user.roles = profile["roles"]

    return user


def set_dummy_user(*args, **kwargs):
    g.user = {
        "email": "username@cern.ch",
        "first_name": "First Name",
        "last_name": "Last Name",
        "uid": "usernameuid",
        "roles": ["TEST_ROLE"],
    }


def set_dummy_api_key(*args, **kwargs):
    print("Set dummy account_id")
    g.account_id = 1
    g.username = "the_director"
    g.first_name = "Steven"
    g.last_name = "Spielberg"
    g.is_active = True
    g.get_id = lambda: 1
    login_user(g)


def oidc_validate(func):
    """
    Decorator for validation of the auth token
    """

    def function_wrapper(*args, **kwargs):
        if current_app.config["LOGIN_DISABLED"]:
            set_dummy_user()
            return func(*args, **kwargs)
        try:
            auth_header = request.headers["Authorization"]
            token = auth_header.split("Bearer")[1].strip()
            user_info = parse_id_token(token)
            user = get_user_frofile_from_token(user_info)
            if login_user(user):
                logger.debug(f"User {user.username} was logged in")
        except (KeyError, ExpiredTokenError, DecodeError) as error:
            logger.warning(f"Authentication error: {error}")
            abort(HTTPStatus.UNAUTHORIZED, message="Authorization Denied")
        return func(*args, **kwargs)

    function_wrapper.__name__ = func.__name__
    return function_wrapper
