import pytz


def change_timezone(current_datetime, timezone):
    # prepare the final timezone
    new_time = current_datetime.astimezone(pytz.timezone(timezone))
    return new_time
