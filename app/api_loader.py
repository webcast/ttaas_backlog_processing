from typing import Tuple

from flask import Blueprint

from app.api.private.v1.load_api import (
    load_api_namespaces as load_api_private_namespaces,
)


def load_api_private_blueprints() -> Tuple[Blueprint]:
    """Loads all the blueprints, adding the development ones if needed

    Returns:
        list[Blueprint]: All the blueprints of the API
    """
    all_blueprints = (load_api_private_namespaces(),)

    return all_blueprints
