import logging
import os
from importlib import import_module
from typing import Optional

import sentry_sdk
from flask import Flask
from flask_cors import CORS
from flask_login import current_user
from sentry_sdk.integrations.flask import FlaskIntegration
from werkzeug.middleware.proxy_fix import ProxyFix

from app.api_loader import load_api_private_blueprints
from app.extensions import cache, celery, db, migrate
from app.services.transcription import transcription_cli
from app.utils.logger import setup_logs

logger = logging.getLogger("webapp.app_factory")


def create_app(
    config_filename: str = "config/config.py",
    static_url_path: str = "/static-files",
):
    """
    Create the application from the configuration file passed as parameter
    :param config_object: The python module that will be used to populate the configuration
    :type config_object: Python module
    :return:
    """
    print("Creating Flask application...", end=" ")
    app = Flask(
        __name__,
        static_url_path=static_url_path,
    )
    app.config.from_pyfile(config_filename)
    print("ok")
    CORS(app)

    setup_logs(
        app,
        "webapp",
        to_file=app.config.get("LOG_FILE_ENABLED", False),
    )

    if app.config["USE_PROXY"]:
        app.wsgi_app = ProxyFix(app.wsgi_app, x_for=1, x_proto=1, x_host=1)

    setup_database(app)

    initialize_models(app)
    # load_cern_openid(app)
    initialize_settings(app)
    init_flask_admin(app)

    print("Initialize cli...", end="")
    # initialize cli, where we add additional commands to the CLI
    initialize_cli(app)
    print("ok")

    init_cache(app)

    load_private_api_v1(app)
    initialize_views(app)
    # init_celery(app)
    init_sentry(app)

    @app.context_processor
    def inject_template_variables():
        is_dev = app.config["IS_DEV"]
        return dict(is_dev=is_dev, current_user=current_user)

    return app


def init_cache(app):
    print("Initialize cache: ", end="")
    cache_config = {"CACHE_TYPE": app.config["CACHE_TYPE"]}

    if app.config["CACHE_ENABLE"]:
        if app.config["CACHE_TYPE"] == "redis":
            print("Initializing Redis Caching... ", end="")
            cache_config = {
                "CACHE_TYPE": "redis",
                "CACHE_REDIS_HOST": app.config.get("CACHE_REDIS_HOST", None),
                "CACHE_REDIS_PASSWORD": app.config.get("CACHE_REDIS_PASSWORD", None),
                "CACHE_REDIS_PORT": app.config.get("CACHE_REDIS_PORT", None),
                "CACHE_DEFAULT_TIMEOUT": app.config["CACHE_DEFAULT_TIMEOUT"],
            }
        elif app.config["CACHE_TYPE"] == "SimpleCache":
            cache_config = {
                "CACHE_TYPE": "SimpleCache",
                "CACHE_DEFAULT_TIMEOUT": app.config["CACHE_DEFAULT_TIMEOUT"],
            }

        else:
            print("Caching disabled... ", end="")
            print("ok")
            return
        print(type(cache_config))
        cache.init_app(app, config=cache_config)
        print("ok")


def initialize_cli(app):
    app.cli.add_command(transcription_cli.transcription_cli)


def setup_database(app):
    """
    Initialize the database and the migrations
    """

    if app.config.get("DB_ENGINE", None) == "postgresql":
        print("Initializing Postgres Database... ", end="")
        user = app.config["DB_USER"]
        password = app.config["DB_PASS"]
        service_name = app.config["DB_SERVICE_NAME"]
        db_port = app.config["DB_PORT"]
        db_name = app.config["DB_NAME"]
        app.config[
            "SQLALCHEMY_DATABASE_URI"
        ] = f"postgresql://{user}:{password}@{service_name}:{db_port}/{db_name}"

    elif app.config.get("DB_ENGINE", None) == "mysql":
        print("Initializing Mysql Database... ", end="")
        user = app.config["DB_USER"]
        password = app.config["DB_PASS"]
        service_name = app.config["DB_SERVICE_NAME"]
        db_port = app.config["DB_PORT"]
        db_name = app.config["DB_NAME"]
        app.config[
            "SQLALCHEMY_DATABASE_URI"
        ] = f"mysql+pymysql://{user}:{password}@{service_name}:{db_port}/{db_name}?charset=utf8mb4"

    else:
        print("Initializing Sqlite Database...", end="")
        _basedir = os.path.abspath(os.path.dirname(__file__))
        app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///" + os.path.join(
            _basedir, "webapp.db"
        )

    app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

    db.init_app(app)
    app.db = db
    print("ok")

    print("Initializing Database Migrations... ", end="")
    migrate.init_app(app, db)
    print("ok")


def initialize_models(app):
    """
    Load all the available models
    """
    with app.app_context():
        print("Loading models... ", end="")
        application_models = [
            "app.models.accounts",
            "app.models.uploads",
        ]
        for module in application_models:
            import_module(module)
        print("ok")


def initialize_settings(app):
    pass


def init_flask_admin(app: Flask) -> None:
    """Initializes the flask admin extension.

    Args:
        application (Flask): The flask application to initialize the flask admin with.
    """
    print("Initializing Admin... ", end="")
    print("ok")


def load_private_api_v1(application: Flask) -> None:
    """Loads the private API v1.
    Args:
        application (Flask): The application to load the API into.
    """
    api_blueprints = load_api_private_blueprints()
    prefix = "/api"
    version = "v1"

    for blueprint in api_blueprints:
        application.register_blueprint(
            blueprint,
            url_prefix=f"{prefix}/private/{version}",
        )


def initialize_views(application: Flask) -> None:
    """
    Initializes all the application blueprints

    :param application: Application where the blueprints will be registered
    :return:
    """
    print("initiliaze views")


def init_celery(
    app: Optional[Flask] = None,
    config_filename: str = "config/config.py",
):
    """
    Initialize the Celery tasks support.

    :param app: The current Flask application. If it's not set,
    it will be created using the config parameter
    :param config: Configuration object for the Flask application
    :return: The Celery instance
    """
    print("Initializing Celery... ", end="")
    tasks_modules = [
        "app.tasks.example",
        "app.tasks.reminders",
        "app.tasks.encoders",
        "app.tasks.auto_stop",
        "app.tasks.indico_events",
    ]

    app = app or create_app(config_filename)
    celery.app = app  # type: ignore
    celery.conf.broker_url = app.config["CELERY_BROKER_URL"]  # type: ignore
    celery.conf.result_backend = app.config["CELERY_RESULTS_BACKEND"]  # type: ignore
    celery.conf.beat_schedule = app.config["CELERY_BEAT_SCHEDULE"]
    celery.conf.include = tasks_modules
    celery.conf.task_serializer = "json"
    celery.conf.result_serializer = "json"

    celery.conf.broker_connection_retry_on_startup = True

    print("Adding Celery Liveness Probe... ", end="")
    # celery.steps["worker"].add(LivenessProbe)
    print("ok")

    class ContextTask(celery.Task):
        """Make celery tasks work with Flask app context"""

        def __call__(self, *args, **kwargs):
            if app:
                with app.app_context():
                    return self.run(*args, **kwargs)

    celery.Task = ContextTask
    print("ok")
    return celery


def init_sentry(application: Flask) -> None:
    if application.config["ENABLE_SENTRY"]:
        print("Initializing Sentry... ", end="")
        # pylint: disable=abstract-class-instantiated
        sentry_sdk.init(
            dsn=application.config["SENTRY_DSN"],
            integrations=[FlaskIntegration()],
            environment=application.config["SENTRY_ENVIRONMENT"],
        )
        print("OK")
