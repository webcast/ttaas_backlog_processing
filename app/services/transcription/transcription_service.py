import logging

from requests import ReadTimeout

from app.daos.uploads import UploadDAO
from app.models.uploads import AudioLanguage, TranscriptionStates, Upload
from app.services.transcription.ttaas_service import TtaasService


class TranscriptionService:
    def __init__(self, contribution: Upload, logger=None):
        """

        :param contribution: The contribution involved in the operation
        :type contribution: app.models.events.IndicoEventContribution
        :param logger:
        :type logger:
        """
        self.contribution = contribution
        if logger:
            self.logger = logger
        else:
            self.logger = logging.getLogger("job.service_transcription")

    def request_transcription_for_contribution(self) -> bool:

        api = TtaasService(
            self.contribution.recid,
            self.contribution.namefile,
            self.contribution.id,
            logger=self.logger,
        )

        try:
            response = api.get_language()
            if not response or response == AudioLanguage.OTHER:
                self.logger.debug(
                    f"language not detected or not supported for upload id {self.contribution.id} file {self.contribution.namefile}"
                )

                UploadDAO.set_transcription_status(
                    self.contribution.id,
                    TranscriptionStates.LANGUAGE_NOT_SUPPORTED,
                    "na",
                )
                return False
            self.logger.info(f"got language {response}")

        except Exception:
            self.logger.warning(
                (f"{self.contribution.id}: Error getting language from media. "),
                exc_info=True,
            )
            return False
        try:
            response = api.upload_to_ttaas()
            self.logger.debug(f"TTAAS response: {response}")

            if response and response.status_code == 201:
                UploadDAO.set_transcription_status(
                    self.contribution.id,
                    TranscriptionStates.REQUESTING,
                    response.json()["mediaId"],
                )
                return True
            return False

        except ReadTimeout as error:
            self.logger.warning(
                (
                    f"{self.contribution.id}: Error on transcription request. "
                    f"No response from TTAAS. ({error})"
                ),
                exc_info=True,
            )
            return False

    def request_transcription_update(self) -> bool:
        if (
            self.contribution.state == TranscriptionStates.COMPLETED_CALLBACK_FAILED
            or self.contribution.state == TranscriptionStates.COMPLETED_EMAIL_FAILED
            or self.contribution.state == TranscriptionStates.TRANSLATION_FINISHED
            or self.contribution.state == TranscriptionStates.TRANSCRIPTION_FINISHED
            or self.contribution.state == TranscriptionStates.COMPLETED
        ):
            api = TtaasService(
                self.contribution.recid,
                self.contribution.namefile,
                self.contribution.id,
                logger=self.logger,
            )
            links_dict = api.get_editor_links()
            self.logger.debug(f"we got editor links {links_dict}.")
            if links_dict:
                UploadDAO.set_editor_links(
                    self.contribution.mediaid,
                    links_dict["viewUrl"],
                    links_dict["editUrl"],
                )
                self.logger.debug(
                    f"db links commited to db for {self.contribution.mediaid}"
                )
                return True
            else:
                self.logger.debug(
                    f"Error collecting links for {self.contribution.mediaid}"
                )
                return False
        else:
            self.logger.info(
                f"Upload {self.contribution.mediaid} is not in the state to get editors links"
            )
            return False
