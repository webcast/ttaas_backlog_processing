import csv
import logging
import os
import time

import click
from flask.cli import AppGroup

from app.daos.uploads import UploadDAO
from app.models.accounts import Account
from app.models.uploads import Upload
from app.services.transcription.transcription_service import TranscriptionService
from app.services.transcription.ttaas_service import TtaasService

logger = logging.getLogger("webapp.cli")

transcription_cli = AppGroup("transcription")


@transcription_cli.command("import-file")
@click.argument("filename")
@click.argument("account_name", default="cds_qa")
def import_events(filename: str, account_name: str):
    logger.info("Importing events from csv file")
    if os.path.exists(os.path.join("./files", filename)):
        with open(os.path.join("./files", filename)) as file_obj:
            # skip header
            next(file_obj)
            reader_obj = csv.reader(file_obj)
            for row in reader_obj:
                print(row)
                Upload.create(
                    fulloriginalpath=row[2],
                    recid=row[0],
                    title=row[1],
                    account_name=account_name,
                )
    else:
        logger.info(f"nofile at {os.path.join('./files',filename)}")


@transcription_cli.command("create-account")
@click.argument("name_account")
@click.argument("token")
@click.argument("apikey")
def create_account(name_account: str, token: str, apikey: str):
    Account.create(name_account, token, apikey)


@transcription_cli.command("ingest-ttaas")
@click.argument("upload_id")
def ingest_ttaas(upload_id: int):
    upload = Upload.query.filter_by(id=upload_id).first()
    ttaas = TranscriptionService(contribution=upload, logger=logger)

    ttaas.request_transcription_for_contribution()


@transcription_cli.command("ingest-ttaas-jobs")
@click.argument("n_jobs")
@click.argument("account_id")
def ingest_ttaas_jobs(n_jobs: int, account_id: int):
    uploads = UploadDAO.get_by_state_limit("NOT_REQUESTED", n_jobs, account_id)

    for up in uploads:
        ttaas = TranscriptionService(contribution=up, logger=logger)
        ttaas.request_transcription_for_contribution()


@transcription_cli.command("ingest-getlan")
@click.argument("upload_id")
def ingest_getlan(upload_id: int):
    upload = Upload.query.filter_by(id=upload_id).first()
    ttaas = TtaasService(upload.recid, upload.namefile, upload_id, logger)

    return ttaas.get_language()


@transcription_cli.command("ingest-getlan-jobs")
@click.argument("n_jobs")
@click.argument("account_id")
def ingest_getlan_jobs(n_jobs: int, account_id: int):
    uploads = UploadDAO.get_by_nolanguage_limit(n_jobs, account_id)

    for up in uploads:
        ttaas = TtaasService(up.recid, up.namefile, up.id, logger)
        lan = ttaas.get_language()
        logger.debug(f"{up.id}:{up.title} language: {lan}")


@transcription_cli.command("ingest-getvideotype-jobs")
@click.argument("n_jobs")
@click.argument("account_id")
def ingest_getvideotype_jobs(n_jobs: int, account_id: int):
    uploads = UploadDAO.get_by_nolanguage_limit(n_jobs, account_id)

    for up in uploads:
        ttaas = TtaasService(up.recid, up.namefile, up.id, logger)
        extension = ttaas.get_extension()
        logger.debug(f"{extension} for upload id: {up.id}")


@transcription_cli.command("ingest-geteditor")
@click.argument("upload_id")
def ingest_geteditor(upload_id: int):
    upload = Upload.query.filter_by(id=upload_id).first()
    ttaas = TtaasService(upload.recid, upload.namefile, upload_id, logger)
    editors = ttaas.get_editor_links()
    logger.debug(f"{editors} for upload id: {upload.id}")
    UploadDAO.set_editor_links(
        upload.mediaid,
        editors["viewUrl"],
        editors["editUrl"],
    )


@transcription_cli.command("ingest-stats")
@click.argument("account_id")
@click.argument("days", default=0)
def ingest_stats(account_id: int, days: int = 0):
    all_records = UploadDAO.get_all_records_peraccountid(account_id=account_id)
    all_processedok = UploadDAO.get_processed_okrecords_withinterval_peraccountid(
        account_id=account_id
    )
    all_processedok_in_days = (
        UploadDAO.get_processed_okrecords_withinterval_peraccountid(
            account_id=account_id, days=days
        )
    )

    percetangedone = (all_processedok / all_records) * 100
    logger.info(
        f"account id: {account_id} - total records: {all_records} - processed in last days - \
         {all_processedok_in_days} - % {round(percetangedone, 2)}"
    )


@transcription_cli.command("download-captions")
@click.argument("account_id")
@click.argument("mediaid")
@click.argument("counter", required=False, default=0, type=int)
@click.argument("primary_key", required=False, default=0, type=int)
def download_captions(
    account_id: int, mediaid: str = None, counter: int = 0, primary_key: int = 0
):
    logger.info(f"download_captions: {account_id} {mediaid} {counter} {primary_key}")
    if mediaid == "":
        mediaid = None
    if mediaid:
        all_records = [UploadDAO.get_record_by_mediaid(account_id, mediaid)]
    elif primary_key:
        all_records = UploadDAO.get_all_records_per_accountid_gt_id(
            account_id=account_id, id=primary_key
        )
    else:
        all_records = UploadDAO.get_all_records_per_accountid_ok(account_id=account_id)

    logger.info(f"we got {len(all_records)} to process")

    total = 0
    for upload in all_records:
        ttaas = TtaasService(upload.recid, upload.namefile, upload.id, logger)
        logger.info(f"retrieving record {upload.id}")
        if total % 5 == 0:
            time.sleep(5)
        ttaas.get_captions_to_file()
        if counter == 0:
            total += 1
            continue
        elif counter != 0 and total <= int(counter):
            total += 1
        else:
            logger.info(f"all files downloaded: {total}")
            break
