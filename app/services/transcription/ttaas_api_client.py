import logging
import os
import re
import shutil
from time import time
from typing import Optional

import magic
import requests
import tlp_client
from flask import current_app
from requests.exceptions import HTTPError, RequestException, Timeout
from requests_toolbelt import MultipartEncoder


class TtaasApiClient:
    def __init__(
        self,
        service_endpoint: str,
        service_token: str,
        whisper_endpoint: str,
        captions_dir: str,
        logger=None,
    ) -> None:
        if logger:
            self.logger = logger
        else:
            self.logger = logging.getLogger("job.ttaas_api_client")

        self.service_endpoint = service_endpoint
        self.service_token = service_token
        self.whisper_endpoint = whisper_endpoint
        self.captions_dir = captions_dir

    @property
    def ttaas_api_session(self) -> requests.Session:
        session = requests.Session()
        session.headers = {
            "Content-Type": "application/json",
            "Authorization": f"Bearer {self.service_token}",
        }
        session.stream = True

        return session

    def _unique_name(self, file_name):
        # Split the filename and the extension
        name, ext = os.path.splitext(file_name)

        # Generate a unique identifier using seconds since epoch
        unique_id = int(time())

        # Construct the unique filename by appending the unique identifier
        unique_filename = f"{name}_{unique_id}{ext}"

        return unique_filename

    def add_video_extension(self, file):
        # Dictionary mapping container types to their corresponding video extensions
        video_extensions = {
            "video/quicktime": ".mov",
            "video/mp4": ".mp4",
            "application/octet-stream": ".mp4",
            # Add more container types and extensions as needed
        }

        # Initialize magic detector
        mime = magic.Magic(mime=True)

        # Get the MIME type of the file
        mime_type = mime.from_file(file)

        # Check if the container type is supported
        if mime_type not in video_extensions:
            self.logger.debug(f"Unsupported video container type: {mime_type}")
            return None

        # Add the appropriate video extension to the filename
        filename_with_extension = f"{video_extensions[mime_type]}"

        return filename_with_extension

    def upload_to_ttaas(self, video_path: str, data: dict) -> requests.Response:
        print(data["language"])
        if not os.path.exists(video_path):
            self.logger.debug(f"{video_path} doesnt exist")
            return None
        file_name = video_path.split("/")[-1]
        # if no extension get one
        new_file_path = None
        file_name_arr = os.path.splitext(file_name)
        if len(file_name_arr[1]) == 0:
            ext = self.add_video_extension(video_path)
            uniquename = self._unique_name(file_name)

            new_file_path = os.path.join(
                current_app.config["STAGING_AREA"], uniquename + ext
            )
            self.logger.debug(f"new file name is: {new_file_path}")
            shutil.copy(video_path, new_file_path)

        api_path = "/api/public/v1/uploads/ingest/"
        url = f"{self.service_endpoint}{api_path}"

        self.logger.debug(f"Creating MultipartEncoder for: {data['title']}")
        self.logger.debug(
            f"{data['title']} Using file and path: {file_name}, {video_path}. "
        )
        self.logger.debug(f"{data['title']} Using data: {data}. ")

        try:
            encoder = MultipartEncoder(
                fields={
                    "title": data["title"],
                    "language": data["language"].value,
                    "notificationMethod": "CALLBACK",
                    "username": data["username"],
                    "notificationUrl": data["notificationUrl"],
                    "comments": data["comments"],
                    "mediaFile": (
                        uniquename + ext if new_file_path else file_name,
                        open(new_file_path if new_file_path else video_path, "rb"),
                        "application/octet-stream",
                    ),
                }
            )
        except Exception as exception:
            self.logger.error(
                f"Error creating MultipartEncoder: {exception}", exc_info=True
            )
            raise

        self.logger.debug(f"Calling TTAAS API URL: {url} for {data['title']}")

        response = self.ttaas_api_session.post(
            url,
            headers={"Content-Type": encoder.content_type},
            data=encoder,
            verify=False,
        )
        self.logger.debug(
            f"TTAAS API response for {data['title']}: {response.status_code}, {response.text.strip()}"
        )
        if new_file_path:
            os.remove(new_file_path)

        return response

    def get_editor_links(
        self, mediaId: str, usermllp: str, apikey: str
    ) -> dict[str, str]:
        self.logger.debug(f"get editors for mediaId: {mediaId} and user: {usermllp}")
        if re.match(f"^{re.escape(usermllp)}", mediaId):
            api = ApiSpeechClientMLLP(api_user=usermllp, api_key=apikey)
            return api.get_editor_links(mediaId, usermllp)
        else:
            self.logger.info(
                f"media id: {mediaId} and user {usermllp} dont have editors, not processed."
            )
            return {"editUrl": "", "viewUrl": ""}

    def whisper_language(self, video_path: str, data: dict) -> requests.Response:
        file_name = video_path.split("/")[-1]

        api_path = "detect-language?encode=true"
        url = f"{self.whisper_endpoint}{api_path}"

        self.logger.debug(f"Creating MultipartEncoder for: <{data['title']}>")
        self.logger.debug(
            f"<{data['title']}> Using file: <{file_name}>, path: <{video_path}>. "
        )
        self.logger.debug(f"{data['title']} Using data: {data}. ")

        try:
            encoder = MultipartEncoder(
                fields={
                    "audio_file": (
                        file_name,
                        open(video_path, "rb"),
                        "application/octet-stream",
                        # "multipart/form-data",
                    ),
                }
            )
        except Exception as exception:
            self.logger.error(
                f"Error creating MultipartEncoder: {exception}", exc_info=True
            )
            raise

        self.logger.debug(f"Calling TTAAS API URL: {url} for {data['title']}")
        response = requests.post(
            url,
            headers={"Content-Type": encoder.content_type},
            data=encoder,
            verify=False,
        )
        self.logger.debug(
            f"Whisper API response for {data['title']}: {response.status_code}, {response.text.strip()}"
        )

        return response

    def get_txtl_for_mediaid(self, mediaid, language, recid, namefile, headers="yes"):
        api_path = f"/api/public/v1/media-files/{mediaid}/languages/{language}"
        url = f"{self.service_endpoint}{api_path}"
        try:
            response = self.ttaas_api_session.get(url, verify=False, timeout=120)
            self.logger.debug(
                f"TTAAS API response for {mediaid}: {response.status_code}"
            )
            response.raise_for_status()
            # if successful the file should have a name
            content_disposition = response.headers.get("Content-Disposition")
            if content_disposition and "filename=" in content_disposition:
                filename = content_disposition.split("filename=")[-1].strip('"')
                newnamefile = namefile.replace(".mp4", "-mp4")
                filename = f"{recid}_{newnamefile}_{filename}"
                filename = os.path.join(self.captions_dir, filename)
                self.logger.info(f"filename with destination dir is: <{filename}>")
            else:
                self.logger.debug(f"we didnt get a name for url: <{url}>")
                return {
                    "value": None,
                    "error": False,
                    "error-msg": f"No filename on the headers for {url}",
                }
            # Open file in binary mode and write content in chunks
            with open(filename, "wb") as file:
                for chunk in response.iter_content(chunk_size=1024):
                    if chunk:  # Avoid writing keep-alive chunks
                        file.write(chunk)

            self.logger.debug(f"Download {filename} completed without issues.")
            return {"value": filename, "error": False, "error-msg": ""}
        except HTTPError as http_err:
            self.logger.debug(
                f"HTTP error occurred: {http_err}"
            )  # Catch HTTP errors (4xx, 5xx)
        except Timeout:
            self.logger.debug("The request timed out")  # Handle request timeout
        except RequestException as req_err:
            self.logger.debug(
                f"An error occurred during the request: {req_err}"
            )  # Catch other errors (network issues, etc.)
        except Exception as err:
            self.logger.debug(
                f"An unexpected error occurred: {err}"
            )  # Catch any other exceptions


class ApiSpeechClientMLLP:
    def __init__(
        self,
        api_user: Optional[str] = None,
        api_key: Optional[str] = None,
        substitute_user: Optional[str] = None,
    ):
        self.api_base = current_app.config["MLLP_CONFIG"]["API_BASE"]
        self.editor_base = current_app.config["MLLP_CONFIG"]["EDITOR_BASE"]
        self.api_user = api_user
        self.api_secret = api_key

        if api_user and api_key:
            self.api_user = api_user
            self.api_secret = api_key

        self.tlp = tlp_client.TLPSpeechClient(
            self.api_base, self.editor_base, self.api_user, self.api_secret
        )
        self.substitute_user = substitute_user
        if substitute_user == current_app.config["MLLP_ADMIN_ACCOUNT"]:
            self.substitute_user = None

    def get_editor_links(self, media_id: str, username: str):
        edit_url = self.tlp.generate_player_url(
            media_id, username, 100, req_key_lifetime=525600
        )
        view_url = self.tlp.generate_player_url(
            media_id, None, 100, req_key_lifetime=525600
        )
        return {"editUrl": edit_url, "viewUrl": view_url}
