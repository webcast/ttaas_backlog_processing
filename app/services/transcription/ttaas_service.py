import logging
import os

from flask import current_app

from app.daos.account import AccountDAO
from app.daos.uploads import UploadDAO
from app.models.uploads import TranscriptionStates

# from app.helpers.helpers import convert_to_utc
# from app.models.events import IndicoEventContribution
# from app.services.opencast.opencast_service import OpencastService
from app.services.transcription.ttaas_api_client import TtaasApiClient

# from app.services.video_player_link import VideoPlayerLinkService


class TtaasService:
    def __init__(self, recid: str, namefile: str, id: int, logger=None) -> None:
        if logger:
            self.logger = logger
        else:
            self.logger = logging.getLogger("job.ttaas_service")

        # self.downloads_path = path.join(current_app.config["TTAAS_FOLDER_PATH"])
        self.upload = UploadDAO.get_by_recid_and_filename(
            recid=recid, namefile=namefile, id=id
        )
        self.account = AccountDAO.get_by_id(self.upload.account_id)
        self.ttaas_service_username = self.account.name
        self.ttaas_service_callback_url = current_app.config[
            "TTAAS_SERVICE_CALLBACK_URL"
        ]
        self.whisper_endpoint = current_app.config["WHISPER_ENDPOINT"]
        self.api_client = TtaasApiClient(
            current_app.config["TTAAS_SERVICE_ENDPOINT"],
            self.account.token,
            self.whisper_endpoint,
            current_app.config["CAPTIONS_DIR"],
            logger=self.logger,
        )

    @property
    def name_account(self):
        return self.account.name

    def upload_to_ttaas(self):

        language = self.upload.language

        if language == "other":
            UploadDAO.set_transcription_status(
                self.upload.id, TranscriptionStates.LANGUAGE_NOT_SUPPORTED
            )
            return None

        self.logger.debug(f"{self.upload.id}: Language: {language}")

        data = {
            "title": self.upload.title,
            "language": language,
            "username": self.ttaas_service_username,
            "notificationUrl": f"{self.ttaas_service_callback_url}?upload_id={self.upload.id}&namefile={self.upload.namefile}",
            "comments": "Sent using the backlog tool postprocessing workflow",
        }

        self.logger.debug(f"{self.upload.id}: Uploading video to TTAAS: {data}")

        response = self.api_client.upload_to_ttaas(
            os.path.join(self.upload.pathtofile, self.upload.namefile), data
        )

        self.logger.debug(
            f"{self.upload.id}: TTAAS response: {response.status_code} {response.text.strip()}"
        )

        # os.remove(file_path)

        return response

    def get_editor_links(self):
        return self.api_client.get_editor_links(
            self.upload.mediaid, self.account.name, self.account.apikey
        )

    def get_language(self):

        language = UploadDAO.get_language(self.upload.id)
        if not language:
            data = {"title": self.upload.title}
            response = self.api_client.whisper_language(
                os.path.join(self.upload.pathtofile, self.upload.namefile), data
            )
            print(response.text)
            if response.status_code == 200:
                self.logger.info(response.json())
                language = response.json()["language_code"]
                if language == "en" or language == "fr":
                    UploadDAO.set_language(
                        self.upload.id, response.json()["language_code"]
                    )
                else:
                    UploadDAO.set_language(self.upload.id, "other")

            if response.status_code != 200:
                self.logger.debug(
                    f"we couldnt get the languege for upload: {self.upload.id}, recid: {self.upload.recid} \
                             title: <{self.upload.title}>"
                )
                UploadDAO.set_language(self.upload.id, "other")

            self.logger.debug(
                f"{self.upload.id}: TTAAS Whisper response: {response.status_code} {response.text.strip()}"
            )

        return language

    def get_extension(self):
        if not os.path.exists(
            os.path.join(self.upload.pathtofile, self.upload.namefile)
        ):
            self.logger.debug(
                f"file doesnt esist path: {self.upload.pathtofile} filename: {self.upload.namefile}"
            )
            return None
        extension = self.api_client.add_video_extension(
            os.path.join(self.upload.pathtofile, self.upload.namefile)
        )
        self.logger.info(
            f"{self.upload.id}:{self.upload.title} - with extension: {extension}"
        )
        return extension

    def get_captions_to_file(self):
        for lan in ["en", "fr"]:
            result = self.api_client.get_txtl_for_mediaid(
                self.upload.mediaid,
                lan,
                self.upload.recid,
                self.upload.namefile,
                headers="yes",
            )
            if not result["error-msg"]:
                self.logger.info(
                    f"succesfully retrieved {self.upload.mediaid} in {lan} located at file: {result['value']}"
                )
            else:
                self.logger.debug(
                    f"Error  retrieving {self.upload.mediaid} in {lan} error: {result['error-msg']}"
                )
