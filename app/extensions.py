from celery import Celery
from flask_caching import Cache
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
from flask_wtf.csrf import CSRFProtect
from sqlalchemy.orm import DeclarativeBase

#
# Database
#


class Base(DeclarativeBase):
    pass


db = SQLAlchemy(model_class=Base)
migrate = Migrate()
cache = Cache(config={"CACHE_TYPE": "SimpleCache"})
celery = Celery()
# Csrf protection for forms
csrf = CSRFProtect()
